console.log("Hello World");

// every time you see document, that is client side JavaScript, we're actually interacting with the web page

const form = document.querySelector('form'); // can pass in any valid css-selector of an element on page, we want a form
const loadingElement = document.querySelector('.loading');
const API_URL = 'http://localhost:8080/mews'; //define the server we're using; /mew allow us to define the object (in form) to send to the dynamic server

loadingElement.style.display = 'none';  // hide load element


form.addEventListener('submit', (event) => {  // listen to submit button
    event.preventDefault(); // when a form is submitted, the browser automatically tries to send the data, we want to PREVENT that
                            // so we can handle it with JS
    const formData = new FormData(form); //FormData() is built in to the web browser, passes in a reference for the forms
    const name = formData.get('name');
    const content = formData.get('content');        // WHEN THE FORM IS SUBMITTED

    if (name.trim() && content.trim()) {
        errorElement.style.display = 'none';
        form.style.display = 'none';
        loadingElement.style.display = ''; 

        const mew = {   // WE GRAB THE STUFF FROM IT AND PUT IT IN AN OBJECT
            name,
            content
    }

    

        form.style.display = 'none';        // SHOW LOADING SPINNER
        loadingElement.style.display = '';

        fetch(API_URL, {        // ATTEMPT TO SEND THIS DATA TO BACK-END SERVER
            method: 'POST',
            body: JSON.stringify(mew),
            headers: {
                'content-type': 'application/json'
            }
        }).then(response => response.json())
        .then(createdMew => {
            console.log(createdMew);
            form.reset(); //reset form after submitting 
            form.style.display = 'none';
            loadingElement.style.display = 'none';
        });
    } else {
        
    }
})