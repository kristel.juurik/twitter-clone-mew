// dynamic server:

/* 
    at the time of this exercise, I will be using port 5500.
    I want to set up back-end server and tell it to listen
    to a new port:
*/

const express = require('express');
const app = express();
const cors = require('cors'); // bring in the cors module
const monk = require('monk');

const db = monk('localhost/meower'); // "connect to the mongodb on my local machine to a database called meower"
const mews = db.get('mews');

/*
app.get('/', function (req, res) {
  res.send('Hello World')
})
*/

app.use(cors()); // activate cors
app.use(cors.json);


// hey, server, when you get a GET request of the / route, run this function (request, response) for long
app.get('/', (req, res) => {
    // respond with json
    res.json({
        message: 'Meower!'
    })
})

app.get('/mews', (req, res) => {
    mews
        .find()
        .then (mews => {
            res.json(mews);
        })
})

function isValidMew(mew) {
    return mew.name && mew.name.toString().trim() !== '' && 
    mew.content && mew.content.toString().trim() !== '';
}

app.post('/mews', (req, res) => {
    if (isValidMew(req.body)) {
        //insert into db...
        const mew = {
            name: req.body.name.toString(),
            content: req.body.content.toString(),
            created: new Date()

        }

        mews
            .insert(mew)
            .then(createdMew => {
                res.json (createdMew);
            })

    } else {
        res.status(422);
        res.json({
            message: "Hey! Name and Content are required!"
        });
    }
    console.log(req.body); //log what client side sent
})

const PORT = 8080;

app.listen(PORT, () => {
    console.log('Listening on http://localhost:8080');
})

